

full_name = 'Mary Anthonette Pineda'
age = 22
occupation = 'Student'
movie = 'M3gan'
rate = 7
print(f'I am {full_name}, and I am {age} years old, I am a {occupation}, and my rating for {movie} is {rate}%')

num1 = 3
num2 = 4
num3 = 5

print(num1 * num2)
print(num1 < num3)
num2 += num3
print(f'num2: {num2}')